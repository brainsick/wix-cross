#!/bin/zsh

# This script munges the C# and msbuild project files so that they almost work on .NET Core 2.0.
# Please note that ***I have already run this script on the repo***!  It accounts for most of my changes, probably.
exit

for f in **/*.*proj; do sed -i'' -e 's|<Project .*\?>|<Project Sdk="Microsoft.NET.Sdk">|g' "$f"; done
for f in **/*.csproj; do sed -i'' -e "s|<PlatformTarget>.*\?</PlatformTarget>||g" "$f"; done
for f in **/*.csproj; do sed -i'' -e 's|<RequiredTargetFramework>.*\?</RequiredTargetFramework>||g' "$f"; done
for f in **/*.csproj; do sed -i'' -e 's|<TargetFrameworkVersion>.*\?</TargetFrameworkVersion>||g' "$f"; done
for f in **/*.props; do sed -i'' -e 's|<TargetFrameworkVersion.*\?</TargetFrameworkVersion>||g' "$f"; done
for f in **/*.csproj; do sed -i'' -e 's|\(</PropertyGroup>\)|<EnableDefaultCompileItems>false</EnableDefaultCompileItems>\1|g' "$f"; done
for f in **/*.csproj; do sed -i'' -e 's|\(</PropertyGroup>\)|<EnableDefaultEmbeddedResourceItems>false</EnableDefaultEmbeddedResourceItems>\1|g' "$f"; done
for f in **/*.*proj; do sed -i'' -e 's|\(<OutputType>Exe</OutputType>\)|<TargetFramework>netcoreapp2.0</TargetFramework>\1|g' "$f"; done
for f in **/*.*proj; do sed -i'' -e 's|\(<OutputType>Library</OutputType>\)|<TargetFramework>netstandard2.0</TargetFramework>\1|g' "$f"; done
find . -iname "*.*proj" -exec sh -c 'if grep '{}' -e "<TargetFramework>" > /dev/null; then :; else sed -i"" -e "s|\(</PropertyGroup>\)|<TargetFramework>netstandard2.0</TargetFramework>\1|g" "{}"; fi' \;
for f in **/*.props; do sed -i'' -e 's|.*\[MSBuild\]::GetRegistryValueFromView.*||g' "$f"; done
for f in **/*.targets; do sed -i'' -e 's|.*\[MSBuild\]::GetRegistryValueFromView.*||g' "$f"; done
rm -f tools/Nuget.targets
for f in **/*.props; do sed -i'' -e 's|<Import Project="Nuget.targets" />||g' "$f"; done
for f in **/*.*proj; do sed -i'' -e 's|\\dtf\\|\\DTF\\|g' "$f"; done
for f in **/*.csproj; do sed -i'' -e 's|<Import Project=".*\?GetDirectoryNameOfFileAbove.*\?\.targets" />||g' "$f"; done
for f in **/*.cs; do sed -i'' -e 's|\(\[assembly:\)|//\1|g' "$f"; done
mv ./src/DTF/Libraries/WindowsInstaller/customactiondata.cs ./src/DTF/Libraries/WindowsInstaller/CustomActionData.cs 2>/dev/null
mv ./src/tools/wix/componentrow.cs ./src/tools/wix/ComponentRow.cs 2>/dev/null
mv ./src/tools/wix/inscriber.cs ./src/tools/wix/Inscriber.cs 2>/dev/null
pushd tools/src/DocCompiler; dotnet add *.csproj package --version=4.4.0-preview2-25405-01 System.Configuration.ConfigurationManager; popd
pushd tools/src/MdCompiler; dotnet add *.csproj package --version=4.4.0-preview2-25405-01 System.Configuration.ConfigurationManager; popd
pushd tools/src/MsgGen; dotnet add *.csproj package --version=4.4.0-preview2-25405-01 System.CodeDom; popd
pushd tools/src/WixBuild; dotnet add *.csproj package LibGit2Sharp; popd
pushd tools/src/WixBuild; dotnet add *.csproj package Microsoft.Build.Framework; popd
pushd tools/src/WixBuild; dotnet add *.csproj package Microsoft.Build.Utilities.Core; popd
echo tools/src/WixBuild/WixBuild.csproj: comment out libgit2sharp import
pushd tools/src/XsdGen; dotnet add *.csproj package --version=4.4.0-preview2-25405-01 System.CodeDom; popd
pushd src/DTF/Libraries/Resources; dotnet add *.csproj package --version=4.4.0-preview2-25405-01 System.Security.Permissions; popd
pushd src/DTF/Libraries/WindowsInstaller; dotnet add *.csproj package --version=4.4.0-preview2-25405-01 System.Configuration.ConfigurationManager; popd
pushd src/tools/wix; dotnet add *.csproj package --version=4.4.0-preview2-25405-01 System.CodeDom; popd
pushd src/tools/wix; dotnet add *.csproj package System.Runtime.InteropServices; popd

#pragma once
// Copyright (c) .NET Foundation and contributors. All rights reserved. Licensed under the Microsoft Reciprocal License. See LICENSE.TXT file in the project root for full license information.

#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0500
#endif

#ifndef _WIN32_MSI
//#define _WIN32_MSI 200
#define _WIN32_MSI 99999999
#endif

//#define JET_VERSION 0x0501
#define JET_VERSION 0x0600 // JET_OPENTEMPORARYTABLE

#include "../../tools/winterop/mysal.h"
#include <shlwapi.h> // INCLUDE EARLIER.  else shlwapi won't define LWSTDAPI, etc.

#include <intsafe.h>
#define SIZETAdd SizeTAdd
#define SIZETSub SizeTSub
#define SIZETMult SizeTMult
#define DWORD_MAX 0xFFFFFFFF

#define SHORT_MAX SHRT_MAX
#define SHORT_MIN SHRT_MIN
#define USHORT_MAX USHRT_MAX

HRESULT WINAPI IUnknown_QueryService(IUnknown *punk, REFGUID guidService, REFIID riid, void **ppvOut); // missing from shlwapi.h


#include <windows.h>
#include <windowsx.h>
#include <intsafe.h>
#include <strsafe.h>
#include <wininet.h>
#include <msi.h>
#include <msiquery.h>
#include <psapi.h>
#include <shlobj.h>
#include <shlwapi.h>
#include <gdiplus.h>
#include <tlhelp32.h>
#include <lm.h>
#include <iads.h>
#include <activeds.h>
#include <richedit.h>
#include <stddef.h>
#include <esent.h>
//#include <ahadmin.h>
#include <srrestoreptapi.h>
#include <userenv.h>
#include <winioctl.h>
#include <wtsapi32.h>
//#include <wuapi.h>
#include <commctrl.h>

#include "dutil.h"
#include "aclutil.h"
#include "atomutil.h"
#include "buffutil.h"
#include "butil.h"
#include "cabcutil.h"
#include "cabutil.h"
#include "conutil.h"
#include "cryputil.h"
#include "eseutil.h"
#include "dirutil.h"
#include "dlutil.h"
#include "fileutil.h"
#include "gdiputil.h"
#include "dictutil.h"
#include "inetutil.h"
#include "iniutil.h"
#include "jsonutil.h"
#include "locutil.h"
#include "logutil.h"
#include "memutil.h"  // NOTE: almost everying is inlined so there is a small .cpp file
//#include "metautil.h" - see metautil.cpp why this *must* be commented out
#include "osutil.h"
#include "pathutil.h"
#include "perfutil.h"
#include "polcutil.h"
#include "procutil.h"
#include "regutil.h"
#include "resrutil.h"
#include "reswutil.h"
#include "rmutil.h"
#include "rssutil.h"
#include "apuputil.h" // NOTE: this must come after atomutil.h and rssutil.h since it uses them.
//#include "sqlutil.h" - see sqlutil.cpp why this *must* be commented out
#include "shelutil.h"
#include "srputil.h"
#include "strutil.h"
#include "timeutil.h"
#include "timeutil.h"
#include "thmutil.h"
#include "uriutil.h"
#include "userutil.h"
#include <comutil.h>  // This header is needed for msxml2.h to compile correctly
#include <msxml2.h>   // This file is needed to include xmlutil.h
#include "wiutil.h"
#include "wuautil.h"
#include "xmlutil.h"


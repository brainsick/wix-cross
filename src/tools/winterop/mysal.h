#pragma once

// a few things from sal.h, with _Upperstart_ -> __lowerstart in #defines:
#define __out_range(low, hi)






// SAL:
/* #ifdef __GNUC__ */
/* #  define __inner_checkReturn __attribute__((warn_unused_result)) */
/* #elif defined(_MSC_VER) */
/* #  define __inner_checkReturn __declspec("SAL_checkReturn") */
/* #else */
/* #  define __inner_checkReturn */
/* #endif */

/* #define __checkReturn __inner_checkReturn */

/* /\* Pointer parameters *\/ */
/* #define _in_ */
/* #define _out_ */
/* #define _inout_ */
/* #define _in_z_ */
/* #define _inout_z_ */
/* #define _in_reads_(s) */
/* #define _in_reads_bytes_(s) */
/* #define _in_reads_z_(s) */
/* #define _in_reads_or_z_(s) */
/* #define _out_writes_(s) */
/* #define _out_writes_bytes_(s) */
/* #define _out_writes_z_(s) */
/* #define _inout_updates_(s) */
/* #define _inout_updates_bytes_(s) */
/* #define _inout_updates_z_(s) */
/* #define _out_writes_to_(s,c) */
/* #define _out_writes_bytes_to_(s, c) */
/* #define _out_writes_all_(s) */
/* #define _out_writes_bytes_all_(s) */
/* #define _inout_updates_to_(s, c) */
/* #define _inout_updates_bytes_to_(s, c) */
/* #define _inout_updates_all_(s) */
/* #define _inout_updates_bytes_all_(s) */
/* #define _in_reads_to_ptr_(p) */
/* #define _in_reads_to_ptr_z_(p) */
/* #define _out_writes_to_ptr_(p) */
/* #define _out_writes_to_ptr_z(p) */

/* /\* Optional pointer parameters *\/ */
/* #define _in_opt_ */
/* #define _out_opt_ */
/* #define _inout_opt_ */
/* #define _in_opt_z_ */
/* #define _inout_opt_z_ */
/* #define _in_reads_opt_(s) */
/* #define _in_reads_bytes_opt_(s) */
/* #define _in_reads_opt_z_(s) */

/* #define _out_writes_opt_(s) */
/* #define _out_writes_opt_z_(s) */
/* #define _inout_updates_opt_(s) */
/* #define _inout_updates_bytes_opt_(s) */
/* #define _inout_updates_opt_z_(s) */
/* #define _out_writes_to_opt_(s, c) */
/* #define _out_writes_bytes_to_opt_(s, c) */
/* #define _out_writes_all_opt_(s) */
/* #define _out_writes_bytes_all_opt_(s) */

/* #define _inout_updates_to_opt_(s, c) */
/* #define _inout_updates_bytes_to_opt_(s, c) */
/* #define _inout_updates_all_opt_(s) */
/* #define _inout_updates_bytes_all_opt_(s) */
/* #define _in_reads_to_ptr_opt_(p) */
/* #define _in_reads_to_ptr_opt_z_(p) */
/* #define _out_writes_to_ptr_opt_(p) */
/* #define _Out_writes_to_ptr_opt_z_(p) */

/* /\* Output pointer parameters *\/ */
/* #define _outptr_ */
/* #define _outptr_opt_ */
/* #define _outptr_result_maybenull_ */
/* #define _outptr_opt_result_maybenull_ */
/* #define _outptr_result_z_ */
/* #define _outptr_opt_result_z_ */
/* #define _outptr_result_maybenull_z_ */
/* #define _outptr_opt_result_maybenull_z_ */
/* #define _com_outptr_ */
/* #define _com_outptr_opt_ */
/* #define _com_outptr_result_maybenull_ */
/* #define _com_outptr_opt_result_maybenull_ */
/* #define _outptr_result_buffer_(s) */
/* #define _outptr_result_bytebuffer_(s) */
/* #define _outptr_opt_result_buffer_(s) */
/* #define _outptr_opt_result_bytebuffer_(s) */
/* #define _outptr_result_buffer_to_(s, c) */
/* #define _outptr_result_bytebuffer_to_(s, c) */
/* #define _outptr_result_bytebuffer_maybenull_(s) */
/* #define _outptr_opt_result_buffer_to_(s, c) */
/* #define _outptr_opt_result_bytebuffer_to_(s, c) */
/* #define _result_nullonfailure_ */
/* #define _result_zeroonfailure_ */
/* #define _outptr_result_nullonfailure_ */
/* #define _outptr_opt_result_nullonfailure_ */
/* #define _outref_result_nullonfailure_ */

/* /\* Output reference parameters *\/ */
/* #define _outref_ */
/* #define _outref_result_maybenull_ */
/* #define _outref_result_buffer_(s) */
/* #define _outref_result_bytebuffer_(s) */
/* #define _outref_result_buffer_to_(s, c) */
/* #define _outref_result_bytebuffer_to_(s, c) */
/* #define _outref_result_buffer_all_(s) */
/* #define _outref_result_bytebuffer_all_(s) */
/* #define _outref_result_buffer_maybenull_(s) */
/* #define _outref_result_bytebuffer_maybenull_(s) */
/* #define _outref_result_buffer_to_maybenull_(s, c) */
/* #define _outref_result_bytebuffer_to_maybenull_(s, c) */
/* #define _outref_result_buffer_all_maybenull_(s) */
/* #define _outref_result_bytebuffer_all_maybenull_(s) */

/* /\* Return values *\/ */
/* #define _ret_z_ */
/* #define _ret_writes_(s) */
/* #define _ret_writes_bytes_(s) */
/* #define _ret_writes_z_(s) */
/* #define _ret_writes_bytes_to_(s, c) */
/* #define _ret_writes_maybenull_(s) */
/* #define _ret_writes_to_maybenull_(s, c) */
/* #define _ret_writes_maybenull_z_(s) */
/* #define _ret_maybenull_ */
/* #define _ret_maybenull_z_ */
/* #define _ret_null_ */
/* #define _ret_notnull_ */
/* #define _ret_writes_bytes_to_(s, c) */
/* #define _ret_writes_bytes_maybenull_(s) */
/* #define _ret_writes_bytes_to_maybenull_(s, c) */

/* /\* Other common annotations *\/ */
/* #define _in_range_(low, hi) */
/* #define _out_range_(low, hi) */
/* #define _ret_range_(low, hi) */
/* #define _deref_in_range_(low, hi) */
/* #define _deref_out_range_(low, hi) */
/* #define _deref_inout_range_(low, hi) */
/* #define _pre_equal_to_(expr) */
/* #define _post_equal_to_(expr) */
/* #define _struct_size_bytes_(size) */

/* /\* Function annotations *\/ */
/* #define _called_from_function_class_(name) */
/* #define _check_return_ __checkReturn */
/* #define _Function_class_(name) */
/* #define _raises_seh_exception_ */
/* #define _maybe_raises_seh_exception_ */
/* #define _must_inspect_result_ */
/* #define _use_decl_annotations_ */

/* /\* success/failure annotations *\/ */
/* #define _always_(anno_list) */
/* #define _on_failure_(anno_list) */
/* #define _return_type_success_(expr) */
/* #define _success_(expr) */

/* #define _reserved_ */
/* #define _const_ */

/* /\* Buffer properties *\/ */
/* #define _readable_bytes_(s) */
/* #define _readable_elements_(s) */
/* #define _writable_bytes_(s) */
/* #define _writable_elements_(s) */
/* #define _null_terminated_ */
/* #define _nullnull_terminated_ */
/* #define _pre_readable_size_(s) */
/* #define _pre_writable_size_(s) */
/* #define _pre_readable_byte_size_(s) */
/* #define _pre_writable_byte_size_(s) */
/* #define _post_readable_size_(s) */
/* #define _post_writable_size_(s) */
/* #define _post_readable_byte_size_(s) */
/* #define _post_writable_byte_size_(s) */

/* /\* Field properties *\/ */
/* #define _field_size_(s) */
/* #define _field_size_full_(s) */
/* #define _field_size_full_opt_(s) */
/* #define _field_size_opt_(s) */
/* #define _field_size_part_(s, c) */
/* #define _field_size_part_opt_(s, c) */
/* #define _field_size_bytes_(size) */
/* #define _field_size_bytes_full_(size) */
/* #define _field_size_bytes_full_opt_(s) */
/* #define _field_size_bytes_opt_(s) */
/* #define _field_size_bytes_part_(s, c) */
/* #define _field_size_bytes_part_opt_(s, c) */
/* #define _field_z_ */
/* #define _field_range_(min, max) */

/* /\* Structural annotations *\/ */
/* #define _at_(e, a) */
/* #define _at_buffer_(e, i, c, a) */
/* #define _group_(a) */
/* #define _when_(e, a) */

/* /\* Analysis *\/ */
/* #define _analysis_assume_(expr) */
/* #define _analysis_assume_nullterminated_(expr) */




// SPECSTRINGS, with __SAL replaced with __ in #defines.
// ALSO make sure we don't define null type things which were causing clashes at least in dutil.
//#include <sal.h>

#ifdef __cplusplus
#ifndef __nothrow
#define __nothrow __declspec(nothrow)
#endif
extern "C" {
#else
#ifndef __nothrow
#define __nothrow
#endif
#endif

#define __deref_in
#define __deref_in_ecount(size)
#define __deref_in_bcount(size)

#define __deref_in_opt
#define __deref_in_ecount_opt(size)
#define __deref_in_bcount_opt(size)

#define __deref_opt_in
#define __deref_opt_in_ecount(size)
#define __deref_opt_in_bcount(size)

#define __deref_opt_in_opt
#define __deref_opt_in_ecount_opt(size)
#define __deref_opt_in_bcount_opt(size)

#define __out_awcount(expr,size)
#define __in_awcount(expr,size)

/* Renamed __null to SAL__null for avoiding private keyword conflicts between
   gcc and MS world.  */
/* #define __null */
/* #define __notnull */
#define __maybenull
#define __readonly
#define __notreadonly
#define __maybereadonly
#define __valid
#define __notvalid
#define __maybevalid
#define __readableTo(extent)
#define __elem_readableTo(size)
#define __byte_readableTo(size)
#define __writableTo(size)
#define __elem_writableTo(size)
#define __byte_writableTo(size)
#define __deref
#define __pre
#define __post
#define __precond(expr)
#define __postcond(expr)
#define __exceptthat
#define __execeptthat
#define __inner_success(expr)
  //#define __inner_checkReturn
#define __inner_typefix(ctype)
#define __inner_override
#define __inner_callback
#define __inner_blocksOn(resource)
#define __inner_fallthrough_dec
#define __inner_fallthrough
#define __refparam
#define __inner_control_entrypoint(category)
#define __inner_data_entrypoint(category)

#define __ecount(size)
#define __bcount(size)

#define __in
#define __in_opt
#define __in_nz
#define __in_nz_opt
#define __in_z
#define __in_z_opt
#define __in_ecount(size)
#define __in_ecount_nz(size)
#define __in_ecount_z(size)
#define __in_bcount(size)
#define __in_bcount_z(size)
#define __in_bcount_nz(size)
#define __in_ecount_opt(size)
#define __in_bcount_opt(size)
#define __in_ecount_z_opt(size)
#define __in_bcount_z_opt(size)
#define __in_ecount_nz_opt(size)
#define __in_bcount_nz_opt(size)

#define __out
#define __out_ecount(size)
#define __out_z
#define __out_nz
#define __out_nz_opt
#define __out_z_opt
#define __out_ecount_part(size,length)
#define __out_ecount_full(size)
#define __out_ecount_nz(size)
#define __out_ecount_z(size)
#define __out_ecount_part_z(size,length)
#define __out_ecount_full_z(size)
#define __out_bcount(size)
#define __out_bcount_part(size,length)
#define __out_bcount_full(size)
#define __out_bcount_z(size)
#define __out_bcount_part_z(size,length)
#define __out_bcount_full_z(size)
#define __out_bcount_nz(size)

#define __inout
#define __inout_ecount(size)
#define __inout_bcount(size)
#define __inout_ecount_part(size,length)
#define __inout_bcount_part(size,length)
#define __inout_ecount_full(size)
#define __inout_bcount_full(size)
#define __inout_z
#define __inout_ecount_z(size)
#define __inout_bcount_z(size)
#define __inout_nz
#define __inout_ecount_nz(size)
#define __inout_bcount_nz(size)
#define __ecount_opt(size)
#define __bcount_opt(size)
#define __out_opt
#define __out_ecount_opt(size)
#define __out_bcount_opt(size)
#define __out_ecount_part_opt(size,length)
#define __out_bcount_part_opt(size,length)
#define __out_ecount_full_opt(size)
#define __out_bcount_full_opt(size)
#define __out_ecount_z_opt(size)
#define __out_bcount_z_opt(size)
#define __out_ecount_part_z_opt(size,length)
#define __out_bcount_part_z_opt(size,length)
#define __out_ecount_full_z_opt(size)
#define __out_bcount_full_z_opt(size)
#define __out_ecount_nz_opt(size)
#define __out_bcount_nz_opt(size)
#define __inout_opt
#define __inout_ecount_opt(size)
#define __inout_bcount_opt(size)
#define __inout_ecount_part_opt(size,length)
#define __inout_bcount_part_opt(size,length)
#define __inout_ecount_full_opt(size)
#define __inout_bcount_full_opt(size)
#define __inout_z_opt
#define __inout_ecount_z_opt(size)
#define __inout_bcount_z_opt(size)
#define __inout_nz_opt
#define __inout_ecount_nz_opt(size)
#define __inout_bcount_nz_opt(size)
#define __deref_ecount(size)
#define __deref_bcount(size)
#define __deref_out
#define __deref_out_ecount(size)
#define __deref_out_bcount(size)
#define __deref_out_ecount_part(size,length)
#define __deref_out_bcount_part(size,length)
#define __deref_out_ecount_full(size)
#define __deref_out_bcount_full(size)
#define __deref_out_z
#define __deref_out_ecount_z(size)
#define __deref_out_bcount_z(size)
#define __deref_out_nz
#define __deref_out_ecount_nz(size)
#define __deref_out_bcount_nz(size)
#define __deref_inout
#define __deref_inout_ecount(size)
#define __deref_inout_bcount(size)
#define __deref_inout_ecount_part(size,length)
#define __deref_inout_bcount_part(size,length)
#define __deref_inout_ecount_full(size)
#define __deref_inout_bcount_full(size)
#define __deref_inout_z
#define __deref_inout_ecount_z(size)
#define __deref_inout_bcount_z(size)
#define __deref_inout_nz
#define __deref_inout_ecount_nz(size)
#define __deref_inout_bcount_nz(size)
#define __deref_ecount_opt(size)
#define __deref_bcount_opt(size)
#define __deref_out_opt
#define __deref_out_ecount_opt(size)
#define __deref_out_bcount_opt(size)
#define __deref_out_ecount_part_opt(size,length)
#define __deref_out_bcount_part_opt(size,length)
#define __deref_out_ecount_full_opt(size)
#define __deref_out_bcount_full_opt(size)
#define __deref_out_z_opt
#define __deref_out_ecount_z_opt(size)
#define __deref_out_bcount_z_opt(size)
#define __deref_out_nz_opt
#define __deref_out_ecount_nz_opt(size)
#define __deref_out_bcount_nz_opt(size)
#define __deref_inout_opt
#define __deref_inout_ecount_opt(size)
#define __deref_inout_bcount_opt(size)
#define __deref_inout_ecount_part_opt(size,length)
#define __deref_inout_bcount_part_opt(size,length)
#define __deref_inout_ecount_full_opt(size)
#define __deref_inout_bcount_full_opt(size)
#define __deref_inout_z_opt
#define __deref_inout_ecount_z_opt(size)
#define __deref_inout_bcount_z_opt(size)
#define __deref_inout_nz_opt
#define __deref_inout_ecount_nz_opt(size)
#define __deref_inout_bcount_nz_opt(size)
#define __deref_opt_ecount(size)
#define __deref_opt_bcount(size)
#define __deref_opt_out
#define __deref_opt_out_z
#define __deref_opt_out_ecount(size)
#define __deref_opt_out_bcount(size)
#define __deref_opt_out_ecount_part(size,length)
#define __deref_opt_out_bcount_part(size,length)
#define __deref_opt_out_ecount_full(size)
#define __deref_opt_out_bcount_full(size)
#define __deref_opt_inout
#define __deref_opt_inout_ecount(size)
#define __deref_opt_inout_bcount(size)
#define __deref_opt_inout_ecount_part(size,length)
#define __deref_opt_inout_bcount_part(size,length)
#define __deref_opt_inout_ecount_full(size)
#define __deref_opt_inout_bcount_full(size)
#define __deref_opt_inout_z
#define __deref_opt_inout_ecount_z(size)
#define __deref_opt_inout_bcount_z(size)
#define __deref_opt_inout_nz
#define __deref_opt_inout_ecount_nz(size)
#define __deref_opt_inout_bcount_nz(size)
#define __deref_opt_ecount_opt(size)
#define __deref_opt_bcount_opt(size)
#define __deref_opt_out_opt
#define __deref_opt_out_ecount_opt(size)
#define __deref_opt_out_bcount_opt(size)
#define __deref_opt_out_ecount_part_opt(size,length)
#define __deref_opt_out_bcount_part_opt(size,length)
#define __deref_opt_out_ecount_full_opt(size)
#define __deref_opt_out_bcount_full_opt(size)
#define __deref_opt_out_z_opt
#define __deref_opt_out_ecount_z_opt(size)
#define __deref_opt_out_bcount_z_opt(size)
#define __deref_opt_out_nz_opt
#define __deref_opt_out_ecount_nz_opt(size)
#define __deref_opt_out_bcount_nz_opt(size)
#define __deref_opt_inout_opt
#define __deref_opt_inout_ecount_opt(size)
#define __deref_opt_inout_bcount_opt(size)
#define __deref_opt_inout_ecount_part_opt(size,length)
#define __deref_opt_inout_bcount_part_opt(size,length)
#define __deref_opt_inout_ecount_full_opt(size)
#define __deref_opt_inout_bcount_full_opt(size)
#define __deref_opt_inout_z_opt
#define __deref_opt_inout_ecount_z_opt(size)
#define __deref_opt_inout_bcount_z_opt(size)
#define __deref_opt_inout_nz_opt
#define __deref_opt_inout_ecount_nz_opt(size)
#define __deref_opt_inout_bcount_nz_opt(size)

#define __success(expr)
#define __nullterminated
#define __nullnullterminated
#define __reserved
//#define __checkReturn
#define __typefix(ctype)
#define __override
#define __callback
#define __format_string
#define __blocksOn(resource)
#define __control_entrypoint(category)
#define __data_entrypoint(category)

#define __encoded_pointer

#ifndef __fallthrough
#define __fallthrough
#endif

#ifndef __analysis_assume
#define __analysis_assume(expr)
#endif

#ifndef __CLR_OR_THIS_CALL
#define __CLR_OR_THIS_CALL
#endif

#ifndef __CLRCALL_OR_CDECL
#define __CLRCALL_OR_CDECL __cdecl
#endif

#ifndef __STDC_WANT_SECURE_LIB__
#define __STDC_WANT_SECURE_LIB__ 0
#endif

#ifndef _CRT_SECURE_NO_DEPRECATE
#define _CRT_SECURE_NO_DEPRECATE
#endif

#ifndef RC_INVOKED
#ifndef _CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES
#define _CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES 0
#endif
#ifndef _CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES_COUNT
#define _CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES_COUNT 0
#endif
#ifndef _CRT_SECURE_CPP_OVERLOAD_SECURE_NAMES
#define _CRT_SECURE_CPP_OVERLOAD_SECURE_NAMES 0
#endif
#endif

#ifndef DECLSPEC_ADDRSAFE
#if (_MSC_VER >= 1200) && (defined(_M_ALPHA) || defined(_M_AXP64))
#define DECLSPEC_ADDRSAFE  __declspec(address_safe)
#else
#define DECLSPEC_ADDRSAFE
#endif
#endif /* DECLSPEC_ADDRSAFE */

#ifdef __cplusplus
}
#endif

This is an incredibly rough-n-ready port to Linux.  This is a prototype
and not intended to be upstreamed yet.  I reserve the right to "git
rebase -i" this into something much tidier.

Here's an overview of the interesting code:

  * dotnet-core-port.sh: this already did 80% of the porting to .NET
    Core.  The aim is to up this to near 100%.  (I've already run this
    script, and committed the output.  It would be tidier if I were to
    just publish the script itself and then you could run it.)

  * Makefile.  First, update LINUX_RID with your distro; see
    <https://docs.microsoft.com/en-us/dotnet/core/rid-catalog>.  Second,
    sort out dependencies as below.  Third: run "make" to build Wix,
    then finally run "make example" to build an msi.

Dependencies:
  * .NET Core 2.0 SDK for Linux (currently prerelease); get from
     https://www.microsoft.com/net/core/preview

  * dotnet-core-port.sh requires zsh for the globs.

  * The native code (winterop.dll, uica.dll, WixUIExtension.dll) libs
    are built with 32-bit MinGW-w64.  We also require a statically
    winpthreads library
    (on Fedora, sudo dnf install mingw32-winpthreads-static)

  * .NET Core applications compiled (including cross compiled) for
    the Windows target require a slightly-patched Wine to run.  See
    WINE.txt.

Re the Wine dependency: we could probably augment with
https://git.tartarus.org/?p=simon/wix-on-linux.git to remove the Wine
dependency:

  1. You'd want to delete WINDOWS_RID in the Makefile and replace its
     use with LINUX_RID.

  2. winterop.dll needs replaced with libwinterop.so (or maybe it's
     winterop.so).  If that doesn't work, make sure the .so is in the
     correct directory.  It should be next to the .NET application,
     e.g. in bin/Debug/netcoreapp2.0/$LINUX_RID/publish.  Or you could
     set LD_LIBRARY_PATH.  Or you could manually create and depend upon
     a NuGet package, and inside the .nupkg put the .so in the folder
     runtimes/$LINUX_RID/native.

     uica.dll and WixUIExtension.dll should remain as DLLs and not
     become .so files.  That's because these DLLs are executed by the
     MSI installer, not by WiX.

License of my changes: MS-RL, like WiX.  (However Simon Tatham may use
under MIT to the extent he combines it with his above wix-on-linux.)

LINUX_RID = fedora.26-x64
WINDOWS_RID = win7-x86

# Path relative to src/tools/{light,lit}/bin/Debug/netcoreapp2.0/$(WINDOWS_RID)/publish
WINTEROP = ../../../../../../../tools/winterop/winterop.dll

all:
	cd tools/src/DocCompiler && dotnet publish --self-contained -r $(LINUX_RID)
	cd tools/src/DocFromXsd && dotnet publish --self-contained -r $(LINUX_RID)
	cd tools/src/FlattenXml && dotnet publish --self-contained -r $(LINUX_RID)
	cd tools/src/GenerateWixInclude && dotnet publish --self-contained -r $(LINUX_RID)
	cd tools/src/MdCompiler && dotnet publish --self-contained -r $(LINUX_RID)
	cd tools/src/MsgGen && dotnet publish --self-contained -r $(LINUX_RID)
	cd tools/src/TildeToRelative && dotnet publish --self-contained -r $(LINUX_RID)
	cd tools/src/WixBuild && dotnet publish --self-contained -r $(LINUX_RID)
	cd tools/src/XsdGen && dotnet publish --self-contained -r $(LINUX_RID)
	cd tools/src/XsdStitch && dotnet publish --self-contained -r $(LINUX_RID)
	cd src/DTF/Tools/MakeSfxCA && dotnet publish --self-contained -r $(WINDOWS_RID)
	cd src/libs/dutil && make
	cd src/libs/wcautil && make
	cd src/tools/winterop && make
	cd src/ext/UIExtension/ca && make
	cd src/tools/wix && ../../../tools/src/XsdGen/bin/Debug/netcoreapp2.0/$(LINUX_RID)/XsdGen Xsd/wix.xsd Xsd/wix.xsd.cs Microsoft.Tools.WindowsInstallerXml.Serialize
	cd src/tools/wix && ../../../tools/src/MsgGen/bin/Debug/netcoreapp2.0/$(LINUX_RID)/MsgGen Data/messages.xml Data/messages.xml.cs Data/Microsoft.Tools.WindowsInstallerXml.Data.messages.resources
	cd src/tools/candle && dotnet publish --self-contained -r $(WINDOWS_RID)
	cd src/tools/lit && dotnet publish --self-contained -r $(WINDOWS_RID)
	cd src/tools/light && dotnet publish --self-contained -r $(WINDOWS_RID)
	# FIXME why aren't we erroring if any of the candle invocations in the loop fails?
	cd src/ext/UIExtension/wixlib && for FILE in *.wxs; do WINEPREFIX=~/.winedotnetcore /usr/local/bin/wine ../../../tools/candle/bin/Debug/netcoreapp2.0/$(WINDOWS_RID)/candle.exe "$$FILE" -d"bannerBmp=Bitmaps/bannrbmp.bmp" -d"dialogBmp=Bitmaps/dlgbmp.bmp" -d"exclamationIco=Bitmaps/exclamic.ico" -d"infoIco=Bitmaps/info.ico" -d"licenseRtf=license.rtf" -d"newIco=Bitmaps/new.ico" -d"upIco=Bitmaps/up.ico" -I../../ca/inc; done
	cd src/tools/lit/bin/Debug/netcoreapp2.0/$(WINDOWS_RID)/publish && ln -sf $(WINTEROP)
	cd src/ext/UIExtension/wixlib && WINEPREFIX=~/.winedotnetcore /usr/local/bin/wine ../../../tools/lit/bin/Debug/netcoreapp2.0/$(WINDOWS_RID)/publish/lit.exe -o ../wixext/ui.wixlib *.wixobj -loc WixExt_de-de.wxl -loc WixUI_da-DK.wxl -loc WixUI_et-EE.wxl -loc WixUI_hr-HR.wxl -loc WixUI_ko-KR.wxl -loc WixUI_pl-pl.wxl -loc WixUI_sk-SK.wxl -loc WixUI_th-TH.wxl -loc WixUI_zh-TW.wxl -loc WixUI_ar-SA.wxl -loc WixUI_de-de.wxl -loc WixUI_fi-FI.wxl -loc WixUI_hu-HU.wxl -loc WixUI_lt-LT.wxl -loc WixUI_pt-BR.wxl -loc WixUI_sl-SI.wxl -loc WixUI_tr-TR.wxl -loc WixUI_bg-BG.wxl -loc WixUI_el-GR.wxl -loc WixUI_fr-fr.wxl -loc WixUI_it-it.wxl -loc WixUI_lv-LV.wxl -loc WixUI_pt-PT.wxl -loc WixUI_sq-AL.wxl -loc WixUI_uk-UA.wxl -loc WixUI_ca-ES.wxl -loc WixUI_en-us.wxl -loc WixUI_he-IL.wxl -loc WixUI_ja-jp.wxl -loc WixUI_nb-NO.wxl -loc WixUI_ro-RO.wxl -loc WixUI_sr-Latn-CS.wxl -loc WixUI_zh-CN.wxl -loc WixUI_cs-CZ.wxl -loc WixUI_es-es.wxl -loc WixUI_hi-IN.wxl -loc WixUI_kk-KZ.wxl -loc WixUI_nl-NL.wxl -loc WixUI_ru-ru.wxl -loc WixUI_sv-SE.wxl -loc WixUI_zh-HK.wxl -bf -b ../ca
	cd src/ext/UIExtension/wixext && dotnet publish --self-contained -r $(WINDOWS_RID)
	cd src/tools/light/bin/Debug/netcoreapp2.0/$(WINDOWS_RID)/publish && ln -sf $(WINTEROP)
	echo "WOOO! WIX BUILT OK"

.PHONY: all test

EXAMPLE_DIR = test/data/Examples/ExampleTest1
WINE = /usr/local/bin/wine
EXAMPLE_WXS = productwine.wxs

example:
	cd $(EXAMPLE_DIR) && ln -sf ../../../../src/ext/UIExtension/wixext/bin/Debug/netstandard2.0/$(WINDOWS_RID)/WixUIExtension.dll .
	cd $(EXAMPLE_DIR) && WINEPREFIX=~/.winedotnetcore $(WINE) ../../../../src/tools/candle/bin/Debug/netcoreapp2.0/$(WINDOWS_RID)/publish/candle.exe $(EXAMPLE_WXS)
	cd $(EXAMPLE_DIR) && WINEPREFIX=~/.winedotnetcore $(WINE) ../../../../src/tools/light/bin/Debug/netcoreapp2.0/$(WINDOWS_RID)/publish/light.exe -v $(basename $(EXAMPLE_WXS)).wixobj -ext WixUIExtension.dll
	echo "WOOO!!! EXAMPLE BUILT OK"
	# Run the built .msi:
	# pushd $(EXAMPLE_DIR) && WINEPREFIX=~/.winedotnetcore $(WINE) msiexec /i $(basename $(EXAMPLE_WXS)).msi; popd
